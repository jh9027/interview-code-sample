import { IsUUID, IsCurrency } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';

export class SaleDto {
  @IsUUID()
  @ApiProperty({
    example: '414c431b-770a-489f-8b00-ee640d933cab',
    description:
      'Token respresenting a card, as returned by the tokenise endpoint.',
  })
  readonly token: string;

  @IsCurrency()
  @ApiProperty({
    example: 21000,
    description:
      'Amount to charge the card in Euros. String should be currency formatted.',
  })
  readonly amount: string;
}
