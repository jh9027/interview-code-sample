import { IsCreditCard, Matches } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';

export class TokeniseCardDto {
  @IsCreditCard({
    message: 'cardNumber must be a valid credit card number',
  })
  @ApiProperty({
    example: '4111-1111-1111-1111',
    description: 'Valid card number.',
  })
  readonly cardNumber: string;

  @Matches(/^(0?[1-9]|1[0-2])\/(\d{2})$/, {
    message: 'expiryDate must be in the format `mm/yy`',
  })
  @ApiProperty({ example: '12/21', description: 'Expiry date of the card.' })
  readonly expiryDate: string;
}
