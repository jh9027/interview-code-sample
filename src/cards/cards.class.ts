import { ApiProperty } from '@nestjs/swagger';

export class TokenisedCardResp {
  @ApiProperty({
    example: '96de582a-2e9a-4ef6-bb74-1d21082933c7',
    description:
      'Token respresenting the card provided. To be used in future sale transactions.',
  })
  token: string;
}

export class SaleResp {
  @ApiProperty({
    example: 'fa741952-b079-4eb9-8b66-6fdffc4718e6',
    description: 'ID representing the sale transaction.',
  })
  transactionId: string;
}
