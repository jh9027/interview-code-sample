import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CardsController } from './cards.controller';
import { CardsService } from './cards.service';
import { TokenEntity } from './entity/token.entity';
import { LedgerEntity } from './entity/ledger.entity';

@Module({
  imports: [TypeOrmModule.forFeature([TokenEntity, LedgerEntity])],
  providers: [CardsService],
  controllers: [CardsController],
})
export class CardsModule {}
