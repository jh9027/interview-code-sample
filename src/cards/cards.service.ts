import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Connection } from 'typeorm';
import { TokenEntity } from './entity/token.entity';
import { TokeniseCardDto } from './dto/tokeniseCard.dto';
import { LedgerEntity } from './entity/ledger.entity';
import { SaleDto } from './dto/sale.dto';
import * as braintree from 'braintree';
import { config } from '../config';

@Injectable()
export class CardsService {
  /**
   * TODO: Ideally this would be encapsulated in an injectable. This would also
   * mean it could be encapsulated in a way that better suits our use case.
   */

  private braintreeGateway: any;

  constructor(
    @InjectRepository(TokenEntity)
    private tokensRepository: Repository<TokenEntity>,
    @InjectRepository(LedgerEntity)
    private ledgerRepository: Repository<LedgerEntity>,
    private connection: Connection,
  ) {
    this.braintreeGateway = braintree.connect({
      environment: braintree.Environment.Sandbox,
      merchantId: config.braintree.merchantId,
      publicKey: config.braintree.publicKey,
      privateKey: config.braintree.privateKey,
    });
  }

  async create(tokeniseCardDto: TokeniseCardDto): Promise<TokenEntity> {
    const queryRunner = this.connection.createQueryRunner();

    const token = new TokenEntity();
    let savedToken;
    token.cardNumber = tokeniseCardDto.cardNumber;
    token.expiryDate = tokeniseCardDto.expiryDate;

    await queryRunner.connect();
    await queryRunner.startTransaction();
    try {
      savedToken = await queryRunner.manager.save(token);

      /**
       * This is a bit basic, ideally all of this would be done automatically
       * so there's no risk of not doing it somewhere. It wasn't immediately
       * obvious how to do this in TypeORM whilst maintaining transaction
       * safety though.
       */
      const ledgerEntry = new LedgerEntity();
      ledgerEntry.action = 'CREATE_TOKEN';
      ledgerEntry.entityId = savedToken.id;
      ledgerEntry.data = token;
      await queryRunner.manager.save(ledgerEntry);

      await queryRunner.commitTransaction();
    } catch (err) {
      await queryRunner.rollbackTransaction();

      // Rethrow error so that an error response is passed on to the user
      throw err;
    } finally {
      await queryRunner.release();
    }

    return savedToken;
  }

  async performSale(saleDto: SaleDto): Promise<any> {
    const tokenEntity: TokenEntity = await this.tokensRepository.findOneOrFail({
      token: saleDto.token,
    });

    // If the token is actually valid, before we start calling braintree lets
    // make a record that we're doing so.
    const initialLedger = new LedgerEntity();
    initialLedger.action = 'SALE_INITIATED';
    initialLedger.entityId = tokenEntity.id;
    initialLedger.data = {
      saleDto,
    };
    await this.ledgerRepository.save(initialLedger);

    let paymentMethod, sale;
    try {
      // Use the stored card details to create a braintree payment method
      paymentMethod = await this.braintreeGateway.creditCard.create({
        number: tokenEntity.cardNumber,
        expirationDate: tokenEntity.expiryDate,
        customerId: config.braintree.customerId,
      });

      // Make a sale against the created card
      sale = await this.braintreeGateway.transaction.sale({
        amount: saleDto.amount,
        paymentMethodToken: paymentMethod.token,
        customerId: config.braintree.customerId,

        options: {
          submitForSettlement: true,
        },
      });
    } finally {
      // Create a ledger entry to record the result of our interactions with braintree
      const saleLedger = new LedgerEntity();
      saleLedger.action = sale.success ? 'SALE_SUCCEEDED' : 'SALE_FAILED';
      saleLedger.entityId = tokenEntity.id;
      saleLedger.data = { paymentMethod, sale };
      this.ledgerRepository.save(saleLedger);
    }

    return sale;
  }
}
