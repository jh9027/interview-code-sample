import {
  Controller,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { CardsService } from './cards.service';
import { TokeniseCardDto } from './dto/tokeniseCard.dto';
import { SaleDto } from './dto/sale.dto';
import { TokenisedCardResp, SaleResp } from './cards.class';
import { ErrorResp } from '../helpers/error.class';
import { EntityNotFoundError } from 'typeorm/error/EntityNotFoundError';

import {
  ApiCreatedResponse,
  ApiBadRequestResponse,
  ApiConflictResponse,
  ApiOperation,
} from '@nestjs/swagger';

@Controller('cards')
@UsePipes(new ValidationPipe({ transform: true }))
export class CardsController {
  constructor(private readonly cardsService: CardsService) {}

  @ApiOperation({
    summary: 'Tokenise card details for use in future transactions.',
  })
  @ApiCreatedResponse({
    description: 'Card details successfully tokenised.',
    type: TokenisedCardResp,
  })
  @ApiBadRequestResponse({ description: 'Bad request data.', type: ErrorResp })
  @Post('/tokens')
  async tokeniseCard(
    @Body() tokeniseDto: TokeniseCardDto,
  ): Promise<TokenisedCardResp> {
    const tokenEntity = await this.cardsService.create(tokeniseDto);

    return {
      token: tokenEntity.token,
    };
  }

  @ApiOperation({ summary: 'Make a sale against the provided card token.' })
  @ApiCreatedResponse({
    description: 'Sale processed successfully.',
    type: SaleResp,
  })
  @ApiBadRequestResponse({ description: 'Bad request data.', type: ErrorResp })
  @ApiConflictResponse({
    description: 'Unable to process payment.',
    type: ErrorResp,
  })
  @Post('/sales')
  async sale(@Body() saleDto: SaleDto): Promise<SaleResp> {
    let sale;
    try {
      sale = await this.cardsService.performSale(saleDto);
    } catch (err) {
      if (err instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            error: 'Bad Request',
            message: "Provided token doesn't exist",
            statusCode: HttpStatus.BAD_REQUEST,
          },
          HttpStatus.BAD_REQUEST,
        );
      } else {
        // If we don't recognise the error, rethrow so that it becomes an internal server error
        throw err;
      }
    }

    if (!sale.success) {
      // 409 Conflict is used as the user can't alter their request to make it work
      throw new HttpException(
        {
          error: 'Conflict',
          message: sale.message,
          statusCode: HttpStatus.CONFLICT,
        },
        HttpStatus.CONFLICT,
      );
    }

    return Promise.resolve({ transactionId: sale.transaction.id });
  }
}
