import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('ledger')
export class LedgerEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  action: string;

  @Column()
  entityId: number;

  @Column({ type: 'json' })
  data: object;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  created: Date;
}
