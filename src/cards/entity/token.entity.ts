import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  Generated,
  BeforeUpdate,
} from 'typeorm';

@Entity('token')
export class TokenEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @Generated('uuid')
  token: string;

  @Column()
  cardNumber: string;

  @Column()
  expiryDate: string;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  created: Date;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  updated: Date;

  @Column({ type: 'timestamp', default: null })
  deleted: Date;

  @BeforeUpdate()
  updateTimestamp() {
    this.updated = new Date();
  }
}
