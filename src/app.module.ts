import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { CardsModule } from './cards/cards.module';
import { config } from './config';
import { TerminusModule } from '@nestjs/terminus';
import { HealthController } from './health/health.controller';

const { postgres } = config;

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: postgres.host,
      port: postgres.port,
      username: postgres.user,
      password: postgres.password,
      database: postgres.dbName,
      autoLoadEntities: true,
      synchronize: true,
    }),
    TerminusModule,
    CardsModule,
  ],
  controllers: [HealthController],
  providers: [],
})
export class AppModule {
  constructor(private connection: Connection) {}
}
