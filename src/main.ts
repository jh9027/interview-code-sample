require('dotenv').config();

import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const options = new DocumentBuilder()
    .setTitle('Coding exercise')
    .setDescription(
      `An example API that showcases some of the qualities I
      focus on in a production API. It was written over a weekend using
      technology new to me and as such I won\'t claim it to be production
      ready. The API itself has two endpoints. One for tokenising card details for
      later use and the other for accepting payments from a tokenised card.`,
    )
    .setVersion('0.1')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('docs', app, document);

  await app.listen(3000);
}
bootstrap();
