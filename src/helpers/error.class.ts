import { ApiProperty } from '@nestjs/swagger';

export class ErrorResp {
  @ApiProperty({
    example: 400,
    description:
      'The HTTP status code returned, including here for easy access.',
  })
  statusCode: number;

  @ApiProperty({
    example: 'Bad Request',
    description:
      'A summary of the error, normally a string representation of the status code.',
  })
  error: string;

  @ApiProperty({
    example: ['amount must be a currency'],
    description: 'A human readable description of the underlying error.',
  })
  message: string | string[];
}
