interface PGConfig {
  host: string;
  port: number;
  user: string;
  password: string;
  dbName: string;
}

interface BraintreeConfig {
  merchantId: string;
  publicKey: string;
  privateKey: string;
  customerId: string;
}

interface Config {
  postgres: PGConfig;
  braintree: BraintreeConfig;
}

const config: Config = {
  postgres: {
    host: process.env.PG_HOST || 'localhost',
    port: parseInt(process.env.PG_PORT, 10) || 5432,
    user: process.env.PG_USER || 'postgres',
    password: process.env.PG_PASSWORD || 'example',
    dbName: process.env.PG_DB || 'coding_exercise',
  },
  braintree: {
    merchantId: process.env.BRAINTREE_MERCHANT_ID,
    publicKey: process.env.BRAINTREE_PUBLIC_KEY,
    privateKey: process.env.BRAINTREE_PRIVATE_KEY,
    customerId: process.env.BRAINTREE_CUSTOMER_ID,
  },
};

export { config };
