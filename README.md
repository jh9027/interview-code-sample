# Card Tokenisation Coding Exercise

This repository contains a card tokenisation API written for a previous job application. It is now used as a publicly available sample of my work. The original task description was as follows:

- Create an API using your language and framework of choice to tokenise card payment information (if you are unfamiliar with card tokenisation, just look it up :))
- Use this token to process a transaction against a Braintree sandbox account (feel free to use any other processor of your choice - Braintree is easy to get started with, as is Stripe)
- Share your work privately via Github and make it available to demo on the web

This repo contains the full infrastructure definition (using Terraform) as well as the application code (NestJS using TS, Postgres backed). The code is far from perfect and certainly not what I consider production ready but hopefully it gives an idea of what I'm capable of in a short time frame.

## Prerequisites

- [AWS CLI](https://aws.amazon.com/cli/)
- [Docker](https://www.docker.com/get-started)
- [jq CLI](https://stedolan.github.io/jq/download/)

## Local setup

```bash
$ npm install
```

```bash
$ cp .env.example .env
```

You then need to update the details stored in `.env` to reflect your local setup.

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev
```

## Test

```bash
$ cp .env.example ./test/.env
```

You then need to update the details stored in `test/.env` to reflect your testing setup.

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Developer notes

### Validation

Validation of input data, e.g. POST body data, is done using the [NestJS validation pipeline](https://docs.nestjs.com/techniques/validation). It is added at the Controller level as opposed to globally because the latter makes it difficult to do end-to-end testing inclusive of validation. The `transform` option should be set to true so that values are coerced where possible - this makes for a more developer friendly experience.

```typescript
@Controller()
@UsePipes(new ValidationPipe({ transform: true }))
export class PaymentsController {
  ...
}
```

### Persistence

Persistence of data is handled by the [NestJS TypeORM integration](https://docs.nestjs.com/techniques/database) using the Repository patten and backed by PostgreSQL.

A generic ledger system has been created which should be used for recording all payment related actions. This should almost certainly be done as part of a transaction when combined with other database actions. Ideally these would be applied automatically but I left this out of scope given the time frame.
