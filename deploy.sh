#!/usr/bin/env bash

set -e

DOCKER_REPOSITORY="103430462594.dkr.ecr.eu-west-1.amazonaws.com/coding-exercise"
COMMIT_HASH=$(git rev-parse HEAD)

eval $(aws ecr get-login --no-include-email)

# Build, tag and push docker image to ECR. We tag with the commit hash for
# easy roll backs
docker build -t coding-exercise:$COMMIT_HASH .
docker tag coding-exercise:$COMMIT_HASH $DOCKER_REPOSITORY:latest
docker tag coding-exercise:$COMMIT_HASH $DOCKER_REPOSITORY:$COMMIT_HASH
docker push $DOCKER_REPOSITORY:latest
docker push $DOCKER_REPOSITORY:$COMMIT_HASH

IMAGE="$DOCKER_REPOSITORY:$COMMIT_HASH"

TASK_DEFINITION_CONTENTS=$(
  cat ecs-task-definition.json |
  sed -e "s%__IMAGE__%${IMAGE}%"
)

TASK_DEFINITION_TMPFILE=$(mktemp)

echo ${TASK_DEFINITION_CONTENTS} > ${TASK_DEFINITION_TMPFILE}

TASK_DEFINITION_RESULT=$(
  aws ecs register-task-definition --cli-input-json "file://${TASK_DEFINITION_TMPFILE}"
)

aws ecs update-service \
  --cluster coding-exercise-ecs-cluster \
  --service api-service \
  --task-definition $(echo ${TASK_DEFINITION_RESULT} | jq --raw-output '.taskDefinition.taskDefinitionArn') \
  --force-new-deployment \