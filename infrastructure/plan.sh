#!/usr/bin/env bash

set -e

terraform get
terraform plan -out plan.tfplan