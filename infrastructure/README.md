# Infrastructure

## Prerequisites

- [Terraform](https://www.terraform.io/downloads.html) - v0.12 or higher
- [Graphviz](https://graphviz.org/download/)

## Initial setup

```bash
$ terraform init
```

## Deploying infrastructure

There are two utility scripts in this folder, `plan.sh` and `apply.sh`.

```bash
$ ./plan.sh
```

This script runs the Terraform planning step and saves the output to `plan.tfplan`. Before applying this it is essential that you inspect the changes that are to be made.

```bash
$ ./apply.sh
```

This script applies the changes from the above planning step.

**Note: the apply step should only be run once the `plan.tfplan` file has been checked.**

## Diagram

![](graph.svg)

## Known issues

- Config is a bit fragmented and at times duplicated (e.g. between TF and docker build scripts). In reality these would come from a secrets store/env vars in CI so I've not focussed on improving this.
- Redeploying to the same task means rollbacks are hard. We'd ideally want to create a new task definition and docker tag per-deploy so that we can easily roll back to earlier app versions.