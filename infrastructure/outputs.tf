output "ecr_url" {
  value = aws_ecr_repository.coding_exercise_ecr.repository_url
}

output "alb_hostname" {
  value = aws_alb.main.dns_name
}

