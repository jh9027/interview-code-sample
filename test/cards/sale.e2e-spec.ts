require('dotenv').config();

import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../../src/app.module';
import { TypeOrmModule } from '@nestjs/typeorm';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot({
          type: 'postgres',
          host: 'localhost',
          port: 5432,
          username: 'postgres',
          password: 'example',
          database: 'coding_exercise_test',
          autoLoadEntities: true,
          synchronize: true,
        }),
        AppModule,
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  describe('/cards/sale (POST)', () => {
    describe('Bad requests', () => {
      const failure = (
        body: any,
        validatorOverrideFn?: (res: any) => any,
      ) => () => {
        const commonMessageValidator = res => {
          expect(res.body).toHaveProperty('message');
          expect(res.body.message).toContain('token must be an UUID');
          expect(res.body.message).toContain('amount must be a currency');
        };

        return request(app.getHttpServer())
          .post('/cards/sales')
          .type('json')
          .send(body)
          .expect('Content-Type', /json/)
          .expect(400)
          .expect(validatorOverrideFn || commonMessageValidator);
      };

      it(
        'returns a 400 if invalid JSON is provided',
        failure('{', res => {
          expect(res.body).toHaveProperty('message');
          expect(res.body.message).toBe('Unexpected end of JSON input');
        }),
      );
      it('returns a 400 if no body is specified', failure(undefined));
      it(
        'returns a 400 if the body data is the incorrect type',
        failure({ token: 1234, amount: '£200' }),
      );
      it(
        "returns a 400 if the body data doesn't match expectations",
        failure({ token: '1234', amount: 21.34 }),
      );
      it(
        "returns a 400 if the token doesn't exist",
        failure(
          { token: '35d4bbd1-1530-4abc-9994-c3f82299f2ae', amount: '21.34' },
          res => {
            expect(res.body).toHaveProperty('message');
            expect(res.body.message).toBe("Provided token doesn't exist");
          },
        ),
      );
    });

    describe('Valid requests', () => {
      let token;

      beforeAll(async () => {
        const res = await request(app.getHttpServer())
          .post('/cards/tokens')
          .type('json')
          .send({ cardNumber: '4111-1111-1111-1111', expiryDate: '05/20' })
          .expect('Content-Type', /json/)
          .expect(201);

        token = res.body.token;
      });

      it('returns a 201 response if valid data is provided', () => {
        return request(app.getHttpServer())
          .post('/cards/sales')
          .type('json')
          .send({
            token,
            amount: '210.00',
          })
          .expect('Content-Type', /json/)
          .expect(201)
          .expect(res => {
            expect(res.body).toHaveProperty('transactionId');
          });
      });

      it("returns a 400 response if the payment isn't authorised", () => {
        return request(app.getHttpServer())
          .post('/cards/sales')
          .type('json')
          .send({
            token,
            amount: '2100.00', // Braintree declines amounts between 2,000 - 3,000 in sandbox mode
          })
          .expect('Content-Type', /json/)
          .expect(409)
          .expect(res => {
            expect(res.body).toHaveProperty('message');
            expect(res.body.error).toEqual('Conflict');
          });
      });
    });
  });
});
