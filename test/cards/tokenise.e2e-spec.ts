require('dotenv').config();

import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../../src/app.module';
import { TypeOrmModule } from '@nestjs/typeorm';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot({
          type: 'postgres',
          host: 'localhost',
          port: 5432,
          username: 'postgres',
          password: 'example',
          database: 'coding_exercise_test',
          autoLoadEntities: true,
          synchronize: true,
        }),
        AppModule,
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  describe('/cards/tokenise (POST)', () => {
    describe('Bad requests', () => {
      const failure = (
        body: any,
        validatorOverrideFn?: (res: any) => any,
      ) => () => {
        const commonMessageValidator = res => {
          expect(res.body).toHaveProperty('message');
          expect(res.body.message).toContain(
            'cardNumber must be a valid credit card number',
          );
          expect(res.body.message).toContain(
            'expiryDate must be in the format `mm/yy`',
          );
        };

        return request(app.getHttpServer())
          .post('/cards/tokens')
          .type('json')
          .send(body)
          .expect('Content-Type', /json/)
          .expect(400)
          .expect(validatorOverrideFn || commonMessageValidator);
      };

      it(
        'returns a 400 if invalid JSON is provided',
        failure('{', res => {
          expect(res.body).toHaveProperty('message');
          expect(res.body.message).toBe('Unexpected end of JSON input');
        }),
      );
      it('returns a 400 if no body is specified', failure(undefined));
      it(
        'returns a 400 if the body data is the incorrect type',
        failure({ cardNumber: 1234, expiryDate: new Date() }),
      );
      it(
        "returns a 400 if the body data doesn't match expectations",
        failure({ cardNumber: 'foo', expiryDate: 'oct/12' }),
      );
      it(
        'returns a 400 if the expiryDate has an invalid month',
        failure({ expiryDate: '13/12' }),
      );
      it(
        "returns a 400 if the cardNumber isn't a valid card number",
        failure({ cardNumber: '1111-1111-1111-1111' }),
      );
    });

    describe('Valid requests', () => {
      it('returns a 201 response if valid data is provided', () => {
        return request(app.getHttpServer())
          .post('/cards/tokens')
          .type('json')
          .send({ cardNumber: '4111-1111-1111-1111', expiryDate: '05/20' })
          .expect('Content-Type', /json/)
          .expect(201);
      });
    });
  });
});
